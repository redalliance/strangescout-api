## ENV Vars

- `SECRET`: JSONWebToken secret
- `DBURL`: Database URL
- `DBNAME`: Database name
- `MONGOOSEDEBUG`: Mongoose debug mode
- `PORT`: Port to listen on
- `ADMINEMAIL`: Admin account email address
- `ADMINPASSWORD`: Admin account password
- `STATIC`: Directory of static site files